export const HeaderItem = [
  {
    name: "About Us",
    link: "/aboutus",
  },
  {
    name: "Contact Us",
    link: "/contactus",
  },
  {
    name: "Hot Products",
    link: "/hotproducts",
  },
  {
    name: "Trending Vendors",
    link: "/trendingvendors",
  },
  {
    name: "Category",
    link: "/category",
  },
];
export const Auth = [
  {
    name: "Login",
    link: "/auth/login",
  },
  {
    name: "Sign Up",
    link: "/auth/signup",
  },
];
export const HotproductData = [
  {
    name: "T-shirt",
    price: "200",
    brand: "ABC Supplier",
    image:
      "https://images.unsplash.com/photo-1521572163474-6864f9cf17ab?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8dCUyMHNoaXJ0fGVufDB8fDB8fHww",
    id: "1",
    rating: "4",
    description: "White Color T-shirt , woolen t shirt",
    stockItem: "45",
    view: "140",
    qty:1
  },
  {
    name: "speaker",
    price: "2000",
    brand: "A to Z Supplier",
    image:
      "https://np-live-21.slatic.net/kf/Sca1490f0ae144dd7b86fa7af5b3647bbn.jpg_300x0q75.webp",
    id: "2",
    rating: "5",
    description: "Wi-Fi Panorama Bulb Camera dfdfd",
    stockItem: "35",
    view: "145",
    qty:1
  },
  {
    name: "Shirley doll 120cm",
    price: "1500",
    brand: "MNC Supplier",
    image:
      "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJQAvgMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABAIDBQYHAQj/xAA8EAABAwIEAwYDBwIFBQAAAAABAAIDBBEFEiExBkFRBxMiYXGBMkKRFCNSYqGx0RXBctLh8PEIJTNEgv/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A7iiIgIiICIiAiIgIiIPCbLlfa5x0/CXOwWjnNPK9g76bXNYj4W21HmV1Ry4Z/wBQ3DsompMfhaTE4dzOfwn5SfI7IOcNlbOQWPjkHIMO3tyUmEujkbLBI+KVhu2SN5a5p8nDULUwXNcHNJaRsQVk6HFHMIbUeNv4hv8A6oOx8I9qFbQhlJxFergv4aljR3rB+YD4h57+p1XWsNxOjxSlbVYfUxzwuGjmH9+i+XoqinewOEgyu0v5rL4Li2I4DVCpwyodE4fE292v9RsUH0qvVo3CPaLQYxkpcRy0VcdAHHwSH8p5HyK3gHqg9RAiAiIgIiIKkREBERAREQEREBERAUHGcMpcYw6fDq+PvKaojLHt52PT/fRTlSUHyNxxwnWcJ45Jh9SC+I+KnqCNJWcvfkR5LXcp32X15xnwxQcV4S+hrmhrx4oZgPFE7qF8xcVcNV/DeKPoMSjIeLlkgGkrfxBBhqeeSFwdE8tI36fRZyhxcOyslAabaBxs32PL9lg8gHw7oBY+aDcWlsjdLnXUO0LfVbhwp2h4jgRZS4gH1tBtZx+9iH5Tz9D+i5XRV8tM5rTd0Y2HNvos3BVRVMfhIN99NfcIPprAsbw/HaJtXhlU2aPYgaOYejhyKya+XMNxeuwWtbW4XUyQTDQ21a8dHDmF2vgXtCouJmNpanJS4m0C8ObwydSw8/RBvCKnMmZBUipzJmQXEREBERAREQEREBeFUyEAXJsBubrlvGnai6nnkw/hlsUsrCWSV0nijY7oxvznz2HQ8g6ZWVlNRQmasqIaeIfPK8NH1K1at7S+FKQlv9T79w0Ip43P/XZcFxOvq8RqXT4lUzVc5+ed5cR6cgPILGzSuAu4+5KDvh7X+F81v+4ev2b/AFUHHuKOAeM8ONDild9ncf8AwyzRFj43dQbLgxnLnANY8jrsrLp3G+ZrmgG3VBmuI+GqnAqk/ew1dHIfuK6ncHxSjoSPhd5FYfJfQixVoPlhuIXkMduGv8J9QrkNQ1/glHoQLEIKXR5Ujc6F+eNxa4bEKSWm2ZpzRn5mqh8enhQSoa7vvDJ4ZP0cve8fDIyWF7o5GG7XtNi09QseWK7HL8sns5B3Ls27Sm4o6LB8deGV9ssNQdBP5H837rpfeL5DfobgkEbEcl2nsw49di8LMHxiUnEYm/czv/8AYYOv5x+o90HU+8XoeseJlWJUGZREQEREBERAXjuXqvV47ZBzDte4nlpI24Bh8rmS1DM9S9hs5kfQHldcgnkihZ4nNY0DTkAth45qH1PGONTyg/dS5B5Na0aLnFTUvraovfsTZo5NCDKPxKnuWsLnu5WGigSzuEofLqRs3kFYki7qTQ5rG9wpME0DI3vfCJKh+jcx0Z5+qC/mbE2KoqCQ158Mbd/UqPVwudIO7s4Ou6zDmsrLmySNdJZzg3VzgCbDqszwljs3DuItr6WOGYgFroJW3EjSgw2XI5ot8XJevjssnickVZidVVRQCCOWUvZENmX5fVRXMQRoZ5aZ12k2PLyU+KSKoF4zkk5tJ0KhuivsCrTopGHM0H2QZF7LGz9HKy5iogrLjJOCQOZUsgWzMOdnMoLDTfwu9ir9M+Wnnjnge6OWNwcx7d2kbFWHgbtNleppO8Gtsw/VB3zg/iNmP4PHU6NqG+Cdo5O6+6z7JtFxzsunmixmpgYD3LobydAQdP7rqjJdEG8IiICIiAiIgLwr1eFB89dotGIONsVjIIZVWcPWwC5hPSy0VQY5mlovo7kV3Ltgw8f1VkxFu9jDmuHIjRc8jcyptDVNHei4Ads70QaoHNe06i6uUVP3ktpWvEJBu5psW6aevothloYozdsLGn/DsosjCNN0GKZSuYWuLhmHMK7lFy91y46k23UhzbaEWVohBTujY3PdZvuqX5szGMHicbBZCmayGHLu63Lmgs/ZmxN1Kh1BHy6K9PO5/LfkrQDWaynKD1QQp7EZnDxDn5KmnmdGQc5F+e6vYhLFK1pjYGNaMo11d5qMGHLsgztVhVUKVlVHD30Dxds0Oo97KBh8NRNXx08DHOkebAALZuz3EHMfPQvcbOHeMHQjQj9lvUDYmvzsijDj8wYAUF/hrDYsFoGwRm8rvFM/8Tv4Wfjm03WHifopcb9EHU0REBERB4vL+asV1bT0VJLVVMgZFE3M5xXKOI+0qvke4YXlpYQfA4gOeR110CDrxKZgdiNV80VXHvEsdQHQ4vU947YXBH0tay6D2ddpkuLYmzCcZLDNIMsNQ0Zczujht6FBs3abgzsUwE1EDM09IS+w3LOf8+y4LVtL2uAte9wT/vRfVFswtYEHcFcU7TeCpsIqH4thsRdh0hLpWNF+4P8Al/ZBosdU5h7upu9ltHjVzR/cKuWAFmeOzmHYjZQ8+gG43tfb0VUMroiXQkW5sOx9R/dBZliIJ0soz2WJ02WZa+Kp5ZJD8pO/oeaizU5Dj4dkGFmqWw1UVj42E38tFIqK4ywMYxwaGm4s3UX3sd1iMQY4Vsodob3VDRIBcA+iCe4yE5mOaBzPNRnvLn2F3uPuVYdNKBlJspmFt8MryPdAZT5Tmks48h0Xj222Up9lYcLoMvwSD/XA4bCN39l0iFy0TgeC9bUSW0ZGBf1K32nYglw8lOiacuysU0eyysEIy7IOjoTYXOyKzVVMVLTy1E7gyKJpc9x5AILmYaqO2vo3y9yyqgdL+BsjS76XXFeLeMcQxt8jIZX09AD4IY3WzDq48/TZaL/Wa3C65tThsvc1ERuyQDb+fdUda7UcfM07cIpnHu4SHVBHN/Jp9Br6+i5LiU8hc5zgcoGqn0eL/aomTVc3eSSEukc7cm6jcRVVNJSvbTixIsbKDAmQshMp+OTRvkFVg876Sshq2OLXxSB4I8jdeVbC54a0eEABVUVO6qraakhF3SyAHyF9UH1vQ1InpYZSdZI2uPuLqQ4MlaWuyuaRYgi4K0PDcaLI2R3NmgNA9Atmw+v74A3Qc34+7L3sdJiXDEYI+KShH7s/hcoDi15bK1zJGEhzXNILT0I6r61vmAstP437PMM4oa6obajxMDSqjb8fQPHzD9UHz+Hhws/VXGVUjW5ZfvGdR8QUriThzF+GKrucWpi1hNo549Y3+h5ehWLbLpp76IPK+jgrmtkgeBK0acvqoksFQIw19OCWi12uFippDHfEA7zVJZb4XuHugxIw+V7gXtDG+typoibGwNYNANleMbidZD6gJ3LPmc4+6CK/XZeZMgBfopLsjNlIwfDZMYru5bdsbCDLJ0HT1QbRwVROjw8zPbYzvzC/QbLb6aHbRWaGlZExkbGhrGANaByCy9NDtogqp4dtFlII/CqKeLbRZGGHwoNsWr9o8NRNwZibaYnO2PO625aDcj6LaFanhZPE+KVuaN7S1zeoIsUHykcSc2Ex75iblY+U984ea2ri7s/xzh6pmcKV9Vh+YmKopxmAbfQOG4NtOnmtZgoa6aQRw0VQ952aIzdBBsYajI06HUaquRz73dqAt6wXgOeppZTjMOR77ZGtPji9Ttfy1Vb+y6pkJ7nFLM6SwXd9QR+yDSKiVjgZmA92emtvVbTwNg77OxSZhDphaAHkzr7rZcD7LKWjkE1dPLWOHyFuSO/+EHX6rdYcDDbBrNtNtggxGH0rgRutvwqIsAGqppcLyW8NlmaamyW0QS4RZqu2uvGiwVSCNW0FNX076esgjmheLOY9twVyPi/sgs6Sq4Zmazcmll+H2PJdlXlkHyRieHYlg0pixOjmgc35nN8J/wDrZRBUNIuDf3X1piGE0lfGWVETXA+QWh412W4VUOdJDSRXOujbfsg4OZx1VsVAe/Iy7ncmt1J9l1afsypqdxP2Br9eZJH7q9TcLmjGWGiZE0fgZZBzzC+G6+vc11Remgve7h4iPIclv+E4XBh9OKeliDWDe+7j1PmspBhNQN2FZCDCpRu0oI1NDssnTwHTRSafDiOX6LJwUdv+EEamg20WRihs3ZX4acC38KWyIAbIJaEXFkRBQ6NrhYjQ7jqsfLg1I9xcGZSehWTRBiW4LC306K8zDo220/RT7L1BGFLF+EXVQp4/wq+iC2ImjYWVYaAvUQAiIgIiICIiCh0bXfEAfZWn0kTt2j6KQiCC7D4uTAgoWD5VORBEbTMHL9FWImjYKQvLILYYAqg1VogIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIg//9k=",
    id: "3",
    rating: "4.5",
    description: "Shirley doll sjdgsjd sgdjsdjs sjdhsjd",
    stockItem: "20",
    view: "240",
    qty:1
  },
  {
    name: "Macrame Swing (Hammock)",
    price: "2500",
    brand: "Company Pvt. Ltd.",
    image:
      "https://meladigitalbazaar.s3.amazonaws.com/product_images/166747539151713.jpg",
    id: "4",
    rating: "3.5",
    description: "Macrame Swing (Hammock)sdsdsd sdsds ",
    stockItem: "5",
    view: "300",
    qty:1
  },
];
export const TrendingVendorsData = [
  {
    name: "T-shirt",
    price: "200",
    brand: "ABC Supplier",
    image:
      "https://static-01.daraz.com.np/p/375d5e9011015e36458a7dbfcf0bf1e6.jpg_300x0q75.webp",
    id: "1",
  },
  {
    name: "Wi-Fi Panorama Bulb Camera",
    price: "2000",
    brand: "A to Z Supplier",
    image:
      "https://static-01.daraz.com.np/p/5cd33f42f7c77bdc7467763c916462d6.jpg_300x0q75.webp",
    id: "2",
  },
  {
    name: "Shirley doll 120cm",
    price: "1500",
    brand: "MNC Supplier",
    image:
      "https://static-01.daraz.com.np/p/1d9f8f9a53acd34879efefc3c1018ed4.jpg_300x0q75.webp",
    id: "3",
  },
  {
    name: "Macrame Swing (Hammock)",
    price: "2500",
    brand: "Company Pvt. Ltd.",
    image:
      "https://static-01.daraz.com.np/p/87e635a6feec22cf7e00a8d092068c03.jpg_300x0q75.webp",
    id: "4",
  },
];
export const auth = [
  {
    name: "Bhuwan Shrestha",
    contact: "9840170728",
    email: "bhuwan@gmail.com",
    dob: "2054/06/07",
    id: "1",
    token: "15458sdsddfdfdgds/dfds",
    type: "user",
  },
];
