import React from "react";

import { Link, useNavigate } from "react-router-dom";
import { Auth, HeaderItem } from "../../utlis/items";
import image1 from "../../image/Logo.png";
import UserHeader from "./UserHeader";

import { useAppContext } from "../../ContextApi";

import Cart from "../../component/user/cart";
import { Avatar, Badge, Drawer, Image } from "antd";
import {
  MinusSquareOutlined,
  PlusSquareOutlined,
  ShoppingCartOutlined,
} from "@ant-design/icons";

const Index = () => {
  const { appState, updateState } = useAppContext();
  const [open, setOpen] = React.useState(false);
  const[myOrder,setMyOrder]=React.useState([])

  console.log("appState", appState);
  const navigate = useNavigate();

  const handleClick = (e) => {
    console.log("data", e);
    navigate(e);
  };
  const onClose = () => {
    setOpen(false);
  };
  const displayHandler = () => {
    setOpen(true);
  };
  React.useEffect(()=>{
    setMyOrder([...new Set(appState?.addtocard)])


  },[appState?.addtocard])
  return (
    <div className=" ">
      <div className="flex justify-between">
        <div>
          <Link to="/">
            <img
              style={{
                width: "50px",
                height: "60px",
                paddingTop: "15px",
              }}
              src={image1}
              alt=""
            />
          </Link>
        </div>

        <div className="flex gap-20">
          {HeaderItem?.map((item) => (
            <div
              key={item.link}
              className="text-white font-bold"
              onClick={() => handleClick(item.link)}
            >
              {item.name}
            </div>
          ))}
        </div>
        <div className="flex gap-5 items-center">
          <div className=" text-white ">
            <Badge count={myOrder?.length}> 
              <Avatar
                onClick={displayHandler}
                icon={<ShoppingCartOutlined />}
              />
            </Badge>
          </div>
          <div>
            <UserHeader />
          </div>
          {Auth?.map((item) => (
            <div
              key={item.link}
              className="text-white font-bold"
              onClick={() => handleClick(item.link)}
            >
              {item.name}
            </div>
          ))}
        </div>
      </div>
      <div>
        {open && (
          <div>
            <Drawer title="Display item" onClose={onClose} open={open}>
              <div>
                {myOrder.map((item) => (
                  <div>
                    <div>
                      <Image src={item.image} alt="no image" />
                    </div>
                    <div>price:{item.price}</div>
                    <div>quantity:{item.qty}</div>
                    <div className="flex gap-3">
                      <PlusSquareOutlined />

                      <MinusSquareOutlined />
                    </div>
                  </div>
                ))}
              </div>
            </Drawer>
          </div>
        )}
      </div>
    </div>
  );
};

export default Index;
///*appstate?.Addtocard lai myOrder le replace*/
